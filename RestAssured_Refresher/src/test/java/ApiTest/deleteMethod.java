package ApiTest;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;

public class deleteMethod {
  @Test
  public void deleteOperation() {
	  baseURI="https://reqres.in/";
	  given().when().delete("api/users/2").then().statusCode(204);
  }
}
