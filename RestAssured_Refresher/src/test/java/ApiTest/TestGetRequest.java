package ApiTest;


import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TestGetRequest {
  @Test
  public void firstGetMethod() {
	 Response response = RestAssured.get("https://reqres.in/api/users?page=2");
	 int statusCode=response.getStatusCode();
	 System.out.println(statusCode);
	 System.out.println(response.getBody().asString());
	  
  }
}
