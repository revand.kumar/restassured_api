package ApiTest;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;

public class put_PatchMethiod {
  @Test
  public void putMethod() {
	  JSONObject req = new JSONObject();
	  req.put("name", "Revand");
	  req.put("job", "Student");
	  System.out.println(req);

	  
	  
		given()
		.body(req.toJSONString()).when()
		.put("https://reqres.in/api/users/2").
		then().statusCode(200)
		.body("updatedAt", greaterThanOrEqualTo("2023-01-24T10:20:37.822Z"));
  }
  @Test
  public void PatchMethiod () {
  
	  JSONObject req = new JSONObject();
	  req.put("name", "Revand");
	  req.put("job", "Student");
	  System.out.println(req);

	  
	  
		given()
		.body(req.toJSONString()).when()
		.patch("https://reqres.in/api/users/2").
		then().statusCode(200)
		.body("updatedAt", greaterThanOrEqualTo("2023-01-24T10:20:37.822Z"));
  
  }
  }


