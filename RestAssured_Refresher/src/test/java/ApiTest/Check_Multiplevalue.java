package ApiTest;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class Check_Multiplevalue {
	@Test
	public void single_value() {
		baseURI = "https://reqres.in";
		given().get("api/users?page=2").then().body("data.last_name", hasItem("Funke")).body("data.email",
				hasItem("byron.fields@reqres.in"));

	}
	@Test
	public void Multi_value() {
		baseURI = "https://reqres.in";
		given().get("api/users?page=2").then().body("data.last_name", hasItems("Funke","Fields","Edwards")).body("data.email",
				hasItems("byron.fields@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in"));

	}
}
