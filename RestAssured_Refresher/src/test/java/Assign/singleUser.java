package Assign;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class singleUser {
  @Test
	  public void testStatusCode() {
		  given().get("https://reqres.in/api/users/2").then().statusCode(200);
	  }
	  @Test
	  public void testParticularValue() {
		  baseURI="https://reqres.in";
		  given().get("api/users/2").then().body("data.last_name", equalTo("Weaver"));
	  }
  }

